/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function printWelcomeMessage() {
	let userName = prompt('What is your name?');
	let userAge = prompt('How old are you?');
	let userAddress = prompt('Where do you live?');

	alert('Thank you for your input!');

	console.log("Hello, " + userName);
	console.log("You are " + userAge + " years old.");
	console.log("You live in " + userAddress);
}

printWelcomeMessage();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function printArtistsBands() {
	let artistBand1 = "Kimbra"; 
	let artistBand2 = "Kendrick Lamar"; 
	let artistBand3 = "Paramore";
	let artistBand4 = "Metric";
	let artistBand5 = "Mac Miller";

	console.log("1. " + artistBand1);
	console.log("2. " + artistBand2);
	console.log("3. " + artistBand3);
	console.log("4. " + artistBand4);
	console.log("5. " + artistBand5);
}

printArtistsBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function printFaveMovies() {
	let faveMovie1 = "Star Wars: Episode IV -- A New Hope";
	let faveMovie2 = "Amelie";
	let faveMovie3 = "Chungking Express";
	let faveMovie4 = "Everything Everywhere All At Once";
	let faveMovie5 = "Princess and the Frog";

	let faveMovieRating1 = 93;
	let faveMovieRating2 = 89;
	let faveMovieRating3 = 88;
	let faveMovieRating4 = 94;
	let faveMovieRating5 = 85;
	
	const ratingLabel = "Rotten Tomatoes Rating: ";

	console.log("1. " + faveMovie1);
	console.log(ratingLabel+faveMovieRating1+"%");

	console.log("2. " + faveMovie2);
	console.log(ratingLabel+faveMovieRating2+"%");

	console.log("3. " + faveMovie3);
	console.log(ratingLabel+faveMovieRating3+"%");

	console.log("4. " + faveMovie4);
	console.log(ratingLabel+faveMovieRating4+"%");

	console.log("5. " + faveMovie5);
	console.log(ratingLabel+faveMovieRating5+"%");
}

printFaveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
// let printFriends() =
function printFriends() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name: "); 
	let friend2 = prompt("Enter your second friend's name: "); 
	let friend3 = prompt("Enter your third friend's name: ");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}

printFriends();

// console.log(friend1);
// console.log(friend2);